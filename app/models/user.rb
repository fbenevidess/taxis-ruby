require 'digest/md5'

class User < ActiveRecord::Base
  validates :username, presence: true
  validates :password, presence: true

  before_create :encrypt_password
  before_save :generate_access_token

  def self.authenticate(username, password)
    user = User.find_by(username: username, password: Digest::MD5.hexdigest(password))

    if user.present?
      user.generate_access_token!
      user
    end
  end

  def generate_access_token
    begin
      self.access_token = SecureRandom.hex
    end while self.class.exists?(access_token: access_token)
  end

  def generate_access_token!
    update(access_token: generate_access_token)
  end

  private
  def encrypt_password
    self.password = Digest::MD5.hexdigest(self.password)
  end
end
