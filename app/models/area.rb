class Area
  def initialize(options = {})
    @sw = options[:sw].split(',')
    @ne = options[:ne].split(',')
  end

  def sw
    {
      latitude: @sw.first,
      longitude: @sw.last
    }
  end

  def ne
    {
      latitude: @ne.first,
      longitude: @ne.last
    }
  end
end
