class Driver < ActiveRecord::Base
  validates :name, presence: true
  validates :car_plate, presence: true, uniqueness: true

  def self.nearest_of(area)
    Driver.where('(latitude >= ? and latitude <= ?) and (longitude >= ? and longitude <= ?) and available = ?',
      area.sw[:latitude], area.ne[:latitude], area.sw[:longitude], area.ne[:longitude], true)
  end

  def update_status(params)
    update(latitude: params[:latitude], longitude: params[:longitude], available: params[:available])
  end

  def status
    {
      driverId: self.id,
      latitude: self.latitude,
      longitude: self.longitude,
      driverAvailable: self.available
    }
  end
end
