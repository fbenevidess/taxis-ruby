class AuthController < ApplicationController
  def create
    @user = User.authenticate(params[:username], params[:password])
    if @user.present?
      render json: { access_token: @user.access_token }, status: :ok
    else
      head :unauthorized
    end
  end
end
