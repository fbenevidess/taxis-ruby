class ApiController < ApplicationController
  before_filter :restrict_access

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render nothing: true, status: :not_found
  end

  rescue_from ActionController::UnknownFormat do |exception|
    render nothing: true, status: :unsupported_media_type
  end

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      User.exists?(access_token: token)
    end
  end
end
