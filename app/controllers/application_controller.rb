class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  respond_to :json

  rescue_from Exception do |exception|
    render json: { message: exception.message }, status: :bad_request
  end
end
