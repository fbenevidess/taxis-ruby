class DriversController < ApiController
  def create
    @driver = Driver.new(driver_params)
    if @driver.save
      render nothing: true, status: :created, location: status_driver_path(@driver)
    else
      render json: @driver.errors, status: :unprocessable_entity
    end
  end

  def status
    @driver = Driver.find(params[:id])
    respond_with @driver.status.to_hash, status: :ok
  end

  def in_area
    @drivers = Driver.nearest_of(Area.new(params))
    respond_with @drivers.map { |driver| driver.status }, status: :ok
  end

  def update_status
    @driver = Driver.find(params[:id])

    if @driver.update_status(params)
      render nothing: true, status: :ok
    else
      render json: @driver.errors, status: :internal_server_error
    end
  end

  private
  def driver_params
    params.require(:driver).permit(:name, :car_plate)
  end
end
