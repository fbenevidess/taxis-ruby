module Taxis
  module Authentication
    def authenticate
      user = FactoryGirl.create :user
      @request.headers['Authorization'] = "Token token=\"#{user.access_token}\""
    end
  end
end
