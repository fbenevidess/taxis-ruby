describe DriversController, type: :controller do
  before { authenticate }

  describe 'POST create' do
    let(:driver) { FactoryGirl.build :driver }

    context  'when all driver information are ok' do
      before { post :create, { driver: { name: driver.name, car_plate: driver.car_plate } } }

      it 'responds a created status code' do
        expect(response).to be_created
      end

      it 'responds a location header with the status of driver' do
        expect(response.location).not_to be_nil
      end
    end

    context 'when some fields are missing' do
      before { post :create, { driver: { car_plate: driver.car_plate } } }

      it 'responds an unprocessable entity status' do
        expect(response.status).to eql 422
      end

      it 'responds a json with the error' do
        json = JSON.parse(response.body)
        expect(json['name'].first).to eql "can't be blank"
      end
    end
  end

  describe 'GET status' do
    context 'when the format is not json' do
      let!(:driver) { FactoryGirl.create :driver_with_coordinates }

      before do
        request.headers['Accept'] = 'application/xml'
      end

      it 'returns a 415 status code' do
        get :status, { id: driver.id }
        expect(response.status).to eql 415
      end
    end

    context 'when a driver does not exists' do
      before { get :status, { id: 3434 }, { 'Accept' => 'application/json' } }

      it 'responds an empty body' do
        expect(response.body).to be_empty
      end

      it 'responds a not found status code' do
        expect(response).to be_not_found
      end
    end

    context 'when a driver exists' do
      let!(:driver) { FactoryGirl.create :driver_with_coordinates }

      before do
        request.headers['Accept'] = 'application/json'
        get :status, { id: driver.id }
      end

      it 'returns a ok status' do
        expect(response.status).to eql 200
      end

      it 'returns a json with the status representation' do
        json = JSON.parse(response.body)
        expect(json['driverId']).to eql driver.id
      end
    end
  end

  describe 'POST status' do
    context 'when a driver does not exists' do
      before do
        post :update_status, { id: 2312, latitude: -23.60810717, longitude: -46.67500346, available: true }
      end

      it 'responds with a 404 status code' do
        expect(response).to be_not_found
      end

      it 'responds an empty body' do
        expect(response.body).to be_empty
      end
    end

    context 'when the status is complete with lat, long and availability' do
      let!(:driver) { FactoryGirl.create :driver }

      before do
        request.headers['Accept'] = 'application/json'
        post :update_status, { id: driver.id, latitude: -23.60810717, longitude: -46.67500346, available: true }
      end

      it 'responds a ok status' do
        expect(response).to be_ok
      end

      it 'responds an empty body' do
        expect(response.body).to be_empty
      end
    end
  end
end
