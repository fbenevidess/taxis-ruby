describe AuthController, type: :controller do
  before { request.headers['Accept'] = 'application/json' }

  describe 'POST auth' do
    let(:user) { FactoryGirl.create(:user, password: 'pass') }

    context 'when credentials are invalid' do
      before do
        post :create, { username: user.username, password: 'hire-me' }
      end

      it 'responds a 401 status code' do
        expect(response).to be_unauthorized
      end
    end

    context 'when credentials are ok' do
      before do
        post :create, { username: user.username, password: 'pass' }
      end

      it 'responds ok status code' do
        expect(response).to be_ok
      end

      it 'responds a json with access_token' do
        json = JSON.parse(response.body)
        expect(json['access_token']).not_to be_empty
      end
    end
  end
end
