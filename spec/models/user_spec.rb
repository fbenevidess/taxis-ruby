describe User, type: :model do
  let(:user) { FactoryGirl.build :user }
  let!(:user_with_access_token) { FactoryGirl.create(:user, password: 'password') }

  subject { user }

  it { should respond_to(:username) }
  it { should respond_to(:password) }
  it { should respond_to(:access_token) }

  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:password) }

  describe '.create' do
    it 'generates an access_token' do
      expect(user_with_access_token.access_token).not_to be_nil
    end

    it 'encrypts the password' do
      expect(user_with_access_token.password).not_to eql 'password'
    end
  end

  describe '.authenticate' do
    it 'refreshes the token' do
      token = user_with_access_token.access_token
      logged = User.authenticate(user_with_access_token.username, 'password')

      expect(token).not_to eql logged.access_token
    end
  end
end
