describe Driver, type: :model do
  let(:driver) { FactoryGirl.build :driver }
  subject { driver }

  it { should respond_to(:name) }
  it { should respond_to(:car_plate) }
  it { should respond_to(:available) }
  it { should respond_to(:latitude) }
  it { should respond_to(:longitude) }

  it { should validate_uniqueness_of(:car_plate) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:car_plate) }

  it { should be_valid }

  describe '.update_status' do
    context 'when latitude, long and availabilty are present' do
      let(:status) do
        {
          latitude: -23.60810717,
          longitude: -46.67500346,
          available: true
        }
      end

      before { @driver = FactoryGirl.create :driver }

      it 'updates the coordinates of a driver' do
        expect { @driver.update_status(status) }.to change { @driver.latitude }.from(nil).to(status[:latitude])
      end

      it 'updates the longitude of a driver' do
        expect { @driver.update_status(status) }.to change { @driver.longitude }.from(nil).to(status[:longitude])
      end

      it 'updates the availability of a driver' do
        @driver.update_status(status)
        expect(@driver.available).to be_truthy
      end
    end
  end

  describe '.nearest_of' do
    let(:area) do
      Area.new({
        sw: '-23.612474,-46.702746',
        ne: '-23.589548,-46.673392'
      })
    end

    before do
      @statuses = JSON.parse(File.read('spec/factories/json/nearest.json'))
      @statuses.each do |status|
        Driver.create({
          name: FFaker::Name.name,
          car_plate: FFaker::Name.name,
          latitude: status['latitude'],
          longitude: status['longitude'],
          available: FFaker::Boolean.maybe
        })
      end

      FactoryGirl.create :driver_with_coordinates
    end

    it 'returns only nearest of an area' do
      expect(Driver.nearest_of(area)).not_to be_empty
    end

    it 'returns only available drivers' do
      Driver.nearest_of(area).each do |driver|
        expect(driver.available?).to be_truthy
      end
    end
  end
end
