describe Area, type: :model do
  describe '.initialize' do
    let(:options) do
      {
        sw: '-23.612474,-46.702746',
        ne: '-23.589548,-46.673392'
      }
    end

    let(:area) { Area.new(options) }

    it 'creates a latitude for SW' do
      expect(area.sw[:latitude]).to eql '-23.612474'
    end

    it 'creates a longitude for SW' do
      expect(area.sw[:longitude]).to eql '-46.702746'
    end

    it 'create a latitude for NE' do
      expect(area.ne[:latitude]).to eql '-23.589548'
    end

    it 'create a latitude for NE' do
      expect(area.ne[:longitude]).to eql '-46.673392'
    end
  end
end
