FactoryGirl.define do
  factory :driver do
    name { FFaker::Name.name }
    car_plate 'NQU2287'
    available true

    factory :driver_with_coordinates, parent: :driver do
      latitude 33.61891
      longitude -117.92895
    end
  end
end
