FactoryGirl.define do
  factory :user do
    username FFaker::Name.name
    password FFaker::Internet.password
  end
end
