class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string :name, null: false
      t.string :car_plate, null: false, unique: true
      t.boolean :available, null: false
      t.decimal :latitude, null: true, precision: 10, scale: 6
      t.decimal :longitude, null: true, precision: 10, scale: 6

      t.timestamps null: false
    end

    add_index :drivers, :car_plate
  end
end
