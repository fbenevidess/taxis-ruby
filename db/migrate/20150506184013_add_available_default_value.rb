class AddAvailableDefaultValue < ActiveRecord::Migration
  def change
    change_column :drivers, :available, :boolean, default: false
  end
end
