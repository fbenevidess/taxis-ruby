require 'ffaker'

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(username: '99taxis', password: 'hire-me')

statuses = JSON.parse(File.read('spec/factories/json/nearest.json'))
statuses.each do |status|
  Driver.create({
    name: FFaker::Name.name,
    car_plate: FFaker::Name.name,
    latitude: status['latitude'],
    longitude: status['longitude'],
    available: status['driverAvailable']
  })
end

1000.times do
  Driver.create([
    {
      name: FFaker::Name.name,
      car_plate: FFaker::Name.name,
      latitude: -5.72358,
      longitude: -36.04872,
      available: FFaker::Boolean.maybe
    },

    {
      name: FFaker::Name.name,
      car_plate: FFaker::Name.name,
      latitude: -2.56820,
      longitude: -44.31044,
      available: FFaker::Boolean.maybe
    }
  ])
end
