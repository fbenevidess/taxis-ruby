# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "ubuntu/trusty64"
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
  end

  config.vm.synced_folder '.', '/taxis'

  config.ssh.forward_agent = true

  config.vm.network :forwarded_port, guest: 3000, host: 3004
  config.vm.network :forwarded_port, guest: 3306, host: 3309

  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ['cookbooks']

    chef.add_recipe 'apt'
    chef.add_recipe 'git'
    chef.add_recipe 'nodejs'
    chef.add_recipe 'ruby_build'
    chef.add_recipe 'rbenv'
    chef.add_recipe "rbenv::user"
    chef.add_recipe "rbenv::vagrant"

    chef.add_recipe 'mysql::client'
    chef.add_recipe 'mysql::server'

    chef.verbose_logging = true

    chef.json = {
      rbenv: {
        user_installs: [{
          user: 'vagrant',
          rubies: ["2.1.4"],
          global: "2.1.4",
          gems: {
            "2.1.4" => [
              { name: "bundler" }
            ]
          }
        }]
      },
      mysql: {
        server_root_username: 'root',
        server_root_password: 'password',
        server_debian_password: 'password',
        server_repl_password: 'password',
        allow_remote_root: true
      }
    }
  end

end
