Rails.application.routes.draw do
  resources :auth, only: [:create]

  resources :drivers, only: [:create] do
    member do
      get 'status'
      post 'status' => 'drivers#update_status'
    end

    collection do
      get 'inArea' => 'drivers#in_area'
    end
  end
end
